using Microsoft.Win32;
using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Point = OpenCvSharp.Point;
using Window = System.Windows.Window;
using wpfHelp;

namespace src
{
    internal class MainWindow: Window {
        DockPanel dock = new DockPanel();
        StackPanel stack = new StackPanel();
        Image image = new Image();

        // constructor
        public MainWindow()
        {
            this.Title = "Hallo";
            this.Content = this.dock;

            this.dock.Children.Add(this.stack);
            DockPanel.SetDock(this.stack, Dock.Left);
            this.dock.Children.Add(this.image);

            this.stack.Children.Add(new AutoActionButton("öffnen", openImage));
        }

        void openImage()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if(ofd.ShowDialog() == true)
            {
                meinBild = Cv2.ImRead(ofd.FileName);
                myImage.Source = OpenCvSharp.WpfExtensions.BitmapSourceConverter.ToBitmapSource(meinBild);
            }
        }
    }
}
