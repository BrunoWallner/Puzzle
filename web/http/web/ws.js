const HOST_ADDRESS = "@HOST_ADDRESS@";
const HOST_IP = HOST_ADDRESS.split(':')[0];
const WS_PORT = 8001;
const WS_ADDRESS = "ws://" + HOST_IP + ":" + WS_PORT; 

const UID = "@UID@";

// will compile to: const socket = new WebSocket(url)
const socket = new WebSocket(WS_ADDRESS);

socket.addEventListener("open", (event) => {
  socket.send(UID + ":" + "login");
  console.log("logged in");
})

socket.addEventListener("message", (event) => {
  let msg = event.data;
  console.log("got" + msg);

  let tokens = msg.split(':');
  if (tokens.length < 1) {return};

  let request = tokens[0];
  switch(request) {
    case "load_image":
    if (tokens.length < 2) {return};
    let src = tokens[1];
    load_image(src);  
  }
  
})

function requestImage() {
  socket.send(UID + ":" + "request_image");
}
