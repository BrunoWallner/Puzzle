const canvas = document.getElementById("imageCanvas");
const ctx = canvas.getContext("2d");

function load_image(src) {
  console.log("loading image: " + HOST_ADDRESS + src);
  var img = new Image();
  img.onload = function() {
    ctx.drawImage(img, 0, 0);
  }
  img.src = "http://" + HOST_ADDRESS + src;
  // img.src = 'https://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png';
}