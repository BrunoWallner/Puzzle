mod http;
mod listener;

use std::{
    collections::HashMap,
    fs::{self, DirEntry},
    io,
};

const IP: &str = "0.0.0.0:8000";
const ROOT: &str = "web/";

fn main() {
    // let root = get_root().unwrap();
    println!("listening on: {}", IP);
    listener::run(IP).unwrap();
}

// fn get_root() -> io::Result<HashMap<String, Vec<u8>>> {
//     let mut map = HashMap::default();

//     for entry in fs::read_dir(ROOT)? {
//         let entry = entry?;
//         if entry.file_type()?.is_dir() {
//             map.extend(read_folder(entry, "")?);
//         } else {
//             let path = entry.path();
//             // let content = fs::read_to_string(&path)?;
//             let content = fs::read(&path)?;
//             let Ok(name) = entry.file_name().into_string() else {continue};
//             let index = format!("/{}", name);
//             map.insert(index, content);
//         }
//     }

//     Ok(map)
// }

// fn read_folder(entry: DirEntry, base_path: &str) -> io::Result<HashMap<String, Vec<u8>>> {
//     let mut map = HashMap::default();

//     if entry.file_type()?.is_dir() {
//         let Ok(name) = entry.file_name().into_string() else {return Err(io::Error::from(io::ErrorKind::InvalidData))};
//         let base_path = format!("{}/{}", base_path, name);

//         for entry in fs::read_dir(entry.path())? {
//             let entry = entry?;
//             map.extend(read_folder(entry, &base_path)?);
//         }
//     } else {
//         let path = entry.path();
//         let content = fs::read(&path)?;
//         let Ok(name) = entry.file_name().into_string() else {return Err(io::Error::from(io::ErrorKind::InvalidData))};
//         let index = format!("{}/{}", base_path, name);

//         map.insert(index, content);
//     }

//     Ok(map)
// }
