use crate::http;
use std::io::{self, Read, Write};
use std::net::{TcpListener, TcpStream, ToSocketAddrs};
use std::sync::{Arc, Mutex};
use std::fs;

const ROOT: &str = "./web/";

lazy_static::lazy_static! {
    static ref NOT_FOUND_HTML: &'static str = "<html><h1p>NOT FOUND</h1></html>";
    static ref NOT_FOUND: String = format!("HTTP/1.1 404 Not Found\r\nContent-Length: {}\r\n\r\n{}", NOT_FOUND_HTML.len(), *NOT_FOUND_HTML);
    static ref UID: Arc<Mutex<u64>> = Arc::new(Mutex::new(rand::random::<u64>()));
}

const INDEX: &str = "/index.html";

pub fn run<A: ToSocketAddrs>(addr: A) -> Result<(), io::Error> {
    let listener = TcpListener::bind(addr)?;

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                std::thread::spawn(move || {
                    handle_client(stream).unwrap_or_else(|error| {
                        eprintln!("Error handling client: {}", error);
                    });
                });
            }
            Err(error) => {
                eprintln!("Failed to establish connection: {}", error);
            }
        }
    }

    Ok(())
}

fn handle_client(mut stream: TcpStream) -> std::io::Result<()> {
    let mut buffer = Vec::new();
    let mut mutex_uid = UID.lock().unwrap();
    *mutex_uid += 1;
    // let uid = prng(*mutex_uid);
    let uid = rand::random::<u64>();
    *mutex_uid = uid;
    drop(mutex_uid);

    loop {
        let mut chunk = [0_u8; 1024];
        let bytes_read = stream.read(&mut chunk)?;

        if bytes_read == 0 {
            // End of stream
            break;
        }

        // Extend the buffer with the received chunk
        buffer.extend(&chunk[..bytes_read]);

        // Check if the message is complete
        if buffer.ends_with(b"\r\n\r\n") {
            // Process the received message
            match process_message(&buffer, &mut stream, uid) {
                Ok(_) => (),
                // Err(e) => eprintln!("error occured: {:?}", e),
                Err(_) => (),
            };

            // Clear the buffer for the next message
            buffer.clear();
        }
    }

    Ok(())
}

fn get_data(path: &str) -> Option<Vec<u8>> {
    let path = match path {
        "/" => "index.html",
        p => p,
    };
    let path = format!("{}{}", ROOT, path.trim_start_matches('/'));
    fs::read(&path).ok()
}

fn process_message(
    message: &[u8],
    stream: &mut TcpStream,
    uid: u64,
) -> Result<(), io::Error> {
    let msg = String::from_utf8_lossy(message);
    let request: http::Request = msg.parse().unwrap();
    let content_type = get_content_type(&request.path);
    // let path = convert_path(&request.path);
    let path = request.path.clone();
    match request.method {
        http::Method::Get => {
            // let Some(file) = root.get(&path) else {
            let Some(data) = get_data(&path) else {
                stream.write_all(NOT_FOUND.as_bytes())?;
                return Err(io::Error::new(io::ErrorKind::NotFound, path));
            };
            let Some(host_address) = request.get("Host") else {return Err(io::Error::from(io::ErrorKind::InvalidInput))};

            // modify string to include @var@ variables if fully valid utf8
            // otherwise do retarded and unnecessary heap allocation
            let mut data: Vec<u8> = if let Ok(string) = String::from_utf8(data.clone()) {
                include_variables(&string, host_address, &uid.to_string())
                    .as_bytes()
                    .to_vec()
            } else {
                data.to_vec()
            };

            let status_line = "HTTP/1.1 200 OK";
            let response = format!(
                "{}{}{}\r\n\r\n",
                status_line,
                if let Some(content_type) = content_type {
                    format!("\r\nContent-Type: {}", content_type)
                } else {
                    format!("")
                },
                format!("\r\nContent-Length: {}", data.len()),
            );
            let mut response = response.as_bytes().to_vec();
            response.append(&mut data);
            // println!("response: {}", response);

            stream.write_all(&response)?;
        }
        http::Method::Post => (),
    }

    Ok(())
}

fn include_variables(file: &str, host_address: &str, uid: &str) -> String {
    let file = file.replace("@HOST_ADDRESS@", host_address);
    let file = file.replace("@UID@", uid);
    file
}

fn get_content_type(file_name: &str) -> Option<String> {
    let Some(extension) = file_name.split(".").last() else {return None};
    let x = match extension {
        "html" => "text/html",
        "css" => "text/css",
        "js" => "text/javascript",
        "jpg" => "image/jpeg",
        "png" => "image/png",
        _ => return None,
    };
    Some(String::from(x))
}

// fn prng(mut x: u64) -> u64 {
//     x ^= x << 13;
//     x ^= x >> 7;
//     x ^= x << 17;
//     x
// }
