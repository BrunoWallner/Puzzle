using System;
using System.Threading;
using System.Threading.Tasks;

namespace Root
{
	class Root
	{
		public delegate void SendEventHandler(string msg);
		public event SendEventHandler OnSend;
		
		public static void Main()
		{
			WebSocket.Listener wsListener = new WebSocket.Listener(Root.WsHook);
			Console.WriteLine(String.Format("WS:\t {0}", "ws://0.0.0.0:8001"));
			wsListener.Start();

			Thread.Sleep(10000);
		
			wsListener.SendAll("load_image:/example.png");
			Console.WriteLine("sent image load request");

			Thread.Sleep(Timeout.Infinite);
		}

		public void Send(string msg)
		{
			if(this.OnSend != null)
			{
				this.OnSend(msg);
			}
		}

		// MUST STILL USE MUTEXES TO MITIGATE FOR DATA-RACES
		// WILL BE EXECUTED CONCURRENTLY
		static string WsHook(string msg)
		{
			string[] tokens = msg.Split(':');
			if (tokens.Length < 2) {return "error:InvalidInput";}
			
			string uid = tokens[0];
			string request = tokens[1];

			string output = "";
			switch(request)
			{
				case "request_image":
					output += "send_image:";
					// output += Image.Generate();
					string file_name = "puzzle_location";
					output += file_name;
					break;
				default:
					break;
			}

			return output;
		}
	}

	// class Image
	// {
	// 	public static string Generate() {
	// 		int width = 50;
	// 		int height = 50;

	// 		// Color[] colors = new Color[16]
	// 		// {
	// 		// 	new Color(255, 0, 0), new Color(0  , 0, 0), new Color(0  , 0, 0), new Color(0  , 0, 0),
	// 		// 	new Color(255, 0, 0), new Color(0  , 0, 0), new Color(0, 255, 0), new Color(0  , 0, 0),
	// 		// 	new Color(255, 0, 0), new Color(0  , 0, 0), new Color(0  , 0, 0), new Color(0  , 0, 0),
	// 		// 	new Color(255, 0, 0), new Color(255, 0, 0), new Color(255, 0, 0), new Color(255, 0, 0)
	// 		// };

	// 		Random rnd = new Random();
	// 		List<byte> colors = new List<byte>();
	// 		for (int i = 0; i < (width * height); i++)
	// 		{
	// 			colors.Add(Convert.ToByte(rnd.Next(0, 255)));
	// 			colors.Add(Convert.ToByte(rnd.Next(0, 255)));
	// 			colors.Add(Convert.ToByte(rnd.Next(0, 255)));
	// 		}

	// 		string output = "";
	// 		output += width.ToString() + ";" + height.ToString() + ";";

	// 		// @Important: Major Performance hit
	// 		// @Todo: use StringBuilder 
	// 		for (int i = 0; i < width * height; i++)
	// 		{
	// 			string tmp = "";
	// 			for (int j = 0; j < 3; j++) 
	// 			{
	// 				byte color = colors[i * 3 + j];
	// 				tmp += color.ToString();
	// 				if (j < 2) {tmp += ",";}
	// 			}
	// 			output += tmp + ";";
	// 		}

	// 		return output;
	// 	}	
	// }
}
