using System;
using Fleck;
using System.Threading.Channels;
using System.Collections.Generic;

namespace WebSocket
{
	public class Listener
	{
		public const int Port = 8001;

		public delegate string Hook(string msg);
		public Hook hook;

		public ChannelWriter<string> writer;
		ChannelReader<string> reader;

		// public Fleck.IWebSocketConnection socket;
		public Dictionary<string, Fleck.IWebSocketConnection> sockets = new Dictionary<string, Fleck.IWebSocketConnection>();
		
		string ip;
		WebSocketServer listener;

		public Listener(Hook hook) {
			this.hook = hook;
			this.ip = "ws://0.0.0.0:8001/";
			this.listener = new WebSocketServer(this.ip);
			this.listener.RestartAfterListenError = true;
			// stfu
			FleckLog.LogAction = (_, _, _) => {};

			var channel = Channel.CreateUnbounded<string>();
			this.writer = channel.Writer;
			this.reader = channel.Reader;
		}

		public void Start()
		{
			this.listener.Start(socket =>
			{
				socket.OnMessage = message => 
				{
					string msg = message; // make extra sure its a string

					string[] tokens = msg.Split(':');
					if (tokens.Length < 2) {return;}
			
					string uid = tokens[0];
					string request = tokens[1];

					switch(request)
					{
						case "login":
							Console.WriteLine(uid + "\t " + "logged in");
							this.sockets[uid] = socket;
							break;
						default:
							break;
					}

					
					string response = this.hook(msg);
					if (!String.IsNullOrEmpty(response))
					{
						socket.Send(response);
					}
				};
			});
		}

		public void Send(string uid, string msg)
		{
			if (this.sockets[uid] != null)
			{
				this.sockets[uid].Send(msg);
			}
		}

		public void SendAll(string msg)
		{
			foreach (var (uid, socket) in this.sockets)
			{
				if (socket != null)
				{
					socket.Send(msg);
				}
			}
		}
	}	
}